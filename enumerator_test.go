package enumeration

import "testing"

// Setup
type elementTestData struct {
	id uint
	name string
	expectedResult bool
}

func setup() []elementTestData {
	var data []elementTestData

	//aws := elementTestData{id: 0, name: "aws", expectedResult: false} // Is used during constructor
	duplicateAWSName := elementTestData{id: 10, name: "aws", expectedResult: false}
	azure := elementTestData{id: 1, name: "AZURE",expectedResult: true }
	duplicateID := elementTestData{id: 1, name: "UniquePlatform", expectedResult: false}
	google := elementTestData{id: 2, name: "GOOGLE" ,expectedResult: true}
	digital_ocean := elementTestData{id: 3, name: "DIGITAL_OCEAN",expectedResult: true }

	//data = append(data, aws)
	data = append(data,duplicateAWSName )
	data = append(data, azure)
	data = append(data, duplicateID)
	data = append(data, google)
	data = append(data, digital_ocean)
	return data
}

func TestEnumerator_AddElement(t *testing.T) {
	tests := setup()
	var cloudPlatform = NewEnumerator(Element{ID: tests[0].id, Value: tests[0].name})
	for _, test := range tests {
		element := Element{ID: test.id, Value: test.name}
		err := cloudPlatform.AddElement(element)
		if test.expectedResult && err == nil {
			t.Log("success, Element added correctly")
		}
		if test.expectedResult && err != nil {
			t.Logf("failed, Element should have been added but wasn't, failed on %s with exception %s", test.name, err)
		}
		if !test.expectedResult && err != nil {
			t.Log("success, Element shouldn't have been added and wasn't")
		}
	}
}

//func TestEnumerator_Ordinal(t *testing.T) {
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//		want   string
//	}{
//		// TODO: Add test cases.
//	}
//
//	//aws := elementTestData{id: 0, name: "aws", expectedResult: false} // Is used during constructor
//	duplicateAWSName := elementTestData{id: 10, name: "aws", expectedResult: false}
//	azure := elementTestData{id: 1, name: "AZURE",expectedResult: true }
//	duplicateID := elementTestData{id: 1, name: "UniquePlatform", expectedResult: false}
//	google := elementTestData{id: 2, name: "GOOGLE" ,expectedResult: true}
//	digital_ocean := elementTestData{id: 3, name: "DIGITAL_OCEAN",expectedResult: true }
//
//	//data = append(data, aws)
//	tests = append(tests,duplicateAWSName )
//	tests = append(tests, azure)
//	tests = append(tests, duplicateID)
//	tests = append(tests, google)
//	tests = append(tests, digital_ocean)
//
//	var cloudPlatform = NewEnumerator(Element{ID: tests[0].id, Value: tests[0].name})
//
//	for _, test := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			e := Enumerator{
//				items:  tt.fields.items,
//				values: tt.fields.values,
//			}
//			if got := e.Ordinal(tt.args.value); got != tt.want {
//				t.Errorf("Ordinal() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}
//
//func TestEnumerator_Value(t *testing.T) {
//	type fields struct {
//		items  map[uint]string
//		values map[string]uint
//	}
//	type args struct {
//		ID uint
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//		want   string
//	}{
//		// TODO: Add test cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			e := Enumerator{
//				items:  tt.fields.items,
//				values: tt.fields.values,
//			}
//			if got := e.Value(tt.args.ID); got != tt.want {
//				t.Errorf("Value() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}