package enumeration

import (
	"errors"
	"strings"
)

// EnumItem The thing that the Enumerator stores
type Element struct {
	ID    uint
	Value string
}

// Enumerator represents a Enum like type
type Enum interface {
	AddElement(Element) error
	Ordinal() uint
	Value() string
}

// This has to be unique for both int and string.  The goal
// is a "type" aware ENUM like pattern
type Enumerator struct {
	items map[uint]string // Forward mapping
	values map[string]uint // This is the reverse mapping
}

// NewEnumerator FILL ME IN
func NewEnumerator(item Element) *Enumerator {
	var items = make(map[uint]string)
	var values = make(map[string]uint)

	items[item.ID] = strings.ToUpper(item.Value)
	values[strings.ToUpper(item.Value)] = item.ID

	return &Enumerator{items: items, values: values}
}

func (e Enumerator) AddElement(item Element) error {
	// TODO: Does this need to be in a do?
	if _, ok := e.items[item.ID]; ok {
		return errors.New("Element already existed in the Enumeration")
	}
	if _, ok := e.values[strings.ToUpper(item.Value)]; ok {
		return errors.New("Element already existed in the Enumeration")
	}

	e.items[item.ID] = strings.ToUpper(item.Value)
	e.values[strings.ToUpper(item.Value)] = item.ID
	return nil
}
func (e Enumerator) Ordinal(value string) uint {
	return e.values[strings.ToUpper(value)]
}

func (e Enumerator) Value(ID uint) string {
	return e.items[ID]
}


// Example Usage

// CloudPlatform blah
//func main() {
//	aws := Element{ID: 0, Value: "aws" }
//	azure := Element{ID: 1, Value: "AZURE" }
//	google := Element{ID: 2, Value: "GOOGLE" }
//	digital_ocean := Element{ID: 3, Value: "DIGITAL_OCEAN" }
//	var cloudPlatform = NewEnumerator(aws)
//	cloudPlatform.AddElement(azure)
//	cloudPlatform.AddElement(google)
//	cloudPlatform.AddElement(digital_ocean)
//
//	fmt.Println(cloudPlatform.Ordinal("AWS"))
//	fmt.Println(cloudPlatform.Value(cloudPlatform.Ordinal("AWS")))
//
//}

//func (c *CloudPlatform) AddElement(item EnumItem) error {
//
//}

// What is the goal
/*
  TYPE map[uint]interface{}
  TYPE["HASHKEY"] = OBJECT
Examples: CloudPlatforms:
 - AWS
 - Azure
 - Google
 - DigitalOcean

CloudPlatform['AWS'] = 0
CloudPlatform
*/
